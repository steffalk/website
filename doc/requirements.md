# Anforderungen für <https://ftcommunity.de>
(Siehe auch: <https://forum.ftcommunity.de/viewtopic.php?f=23&t=5046>)

# Was soll die neue Website können?

* Die vorhandene Seitensammlung (aka Bilderpool) ersetzen
* Serverseitig auf vorhandenen modernen, gepflegten und gut dokumentierten
  Programmpaketen basieren
* Strenge Datenschutzvorgaben erfüllen
* Den Admins (und Redakteuren) eine unkomplizierte Verwaltung ermöglichen
* Software updates (Server) ohne komplexes Fachwissen ermöglichen
  (also zumindest ohne spezielle Anpassungsfummeleien an vielen verschiedenen
  Stellen)
* Inhalte und Darstellung getrennt verwalten (Bilder in 'ner Art Datenbank, Seitenaussehen in Stylesheets oder wie heißt das?)
* Moderne Browser unterstützen (Firefox, Chrome, ...)
* Smartphone und PC Nutzer ansprechen
* Die Nutzer weder analysieren noch ausspionieren (wenn schon Statistiken zur
  Serveroptimierung, dann bitte anonym)
* Das gewohnte Logo verwenden

Achtung, ab hier könnte es off-topic werden oder schon in die Verfeinerungen abdriften:
# Einteilung in Bereiche 

* ftc-Bereich
    * Startseite
    * Veranstaltungshinweise
    * Impressum
* ft:pedia
* Nutzerinhalte
    * Bilder / Alben (Bilderpool)
    * Downloads
    * wiki
* Forum
* ft-Datenbank
* Chat ??? (siehe <https://forum.ftcommunity.de/viewtopic.php?f=23&t=5043>)
* Benutzerverwaltung

# Anforderungen nach Bereichen
* Startseite: Zeigt bei jedem Zugriff eine andere Auswahl von Bildern
  aus dem Bilderpool an.
  Anzeige umfasst Kategoriename, Konstrukteur, Bild und beide Angaben sind
  als Links ausgeführt und öffnen genau dieses Bild.
  Grober Vorschlag zum Aussehen, siehe
  https://forum.ftcommunity.de/viewtopic.php?f=23&t=5003&p=36605#p36605
  * Der Nutzer muss nicht angemeldet sein.
  * Bilderpool
    * Erlaubt Suche nach Bildern, Themen, Stichwort(en), Upload-Datum,
      Fotograf, Konstrukteur, ...
    * angemeldete und freigeschaltete Benutzer können
      * Bilder hochladen
      * hochgeladene Bilder editieren (Gammakorrektur und so Zeug)
      * Kommentare zu Bildern schreiben
      * eigene Kommentare editieren
      * selbst angelegte Kategorie(n) editieren
      * registrierte Benutzer erhalten Benachrichtigung, sehr ähnlich dem
        Forum, bei
        * neuem Kommentar
        * neuem Bild in bestimmter Kategorieauswahl (inkl. "jedes neue Bild")
      * wählen ob neue Bilder im Suchergebnis komplett angezeigt
         werden (so wie heute) oder lediglich die Kategorie angezeigt wird 
         (mit dem letzten neuen Bild dazu).
         _Mich ärgert der gelegentliche Massenupload. Kommen da mehrere
         zusammen,
         fällt das eine feine Bild vom Kollegen der nicht bei der Convention
         war zwischendrin gar nicht mehr auf oder ich klicke mich ewig durch
         die ewig vielen Seiten ..._
    * Angemeldete Benutzer müssen ihre Bilder nicht erst freischalten lassen.
      Bei Regelverstössen kann diese Option für Nutzer durch Admins erzwungen
      werden.
* Admins/Redakteure können die Website editieren
* Es gibt eine Benutzerverwaltung mit
  * Login/Logout
  * (Selbst-)Registrierung von Benutzern - mit modernstem Botschutz
  * (Selbst-)Verwaltung von Profildaten durch angemeldete Benutzer
  * Mandatory: eMail-Adresse, Nickname - mehr nicht, der Rest ist Option!
  * Begrenzte Anpassungen der Startseite individuell für angemeldete
    Benutzer
    (z. B. Neueste Bilder anstelle der Zufallsauswahl oder die neuesten 5
    Einträge im Forum, oder ...)
  * Vergabe/Verwalten von weitergehenden Rechten durch Admins
    * Schreibrecht im Bilderpool
    * Schreibrecht für die ganze Website ("Redakteur")
  * Zugang zur Rechteverwaltung ("Admin")
  * In einer späteren Ausbaustufe soll das [Forum](https://forum.ftcommunity.de) 
    dieselbe Benutzerverwaltung mit nutzen können ("Single Sign On")

# Zusätzliche Anforderungen
* Übernahme von Daten aus dem alten System
    * Bilder und Videos
    * Texte (Bildbeschreibungen und Kommentare)
    * Benutzerdaten
    * Downloads
    * wiki-Seiten
    * die Mirror-Page von ft-computing und ähnliche Schätze
